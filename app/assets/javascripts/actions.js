/* eslint-disable class-methods-use-this */
// 该文件为通用的滚动加载实现方法


import $ from 'jquery';
import Cookies from 'js-cookie';
import createFlash from '~/flash';
import { s__ } from '~/locale';
import { localTimeAgo } from './lib/utils/datetime_utility';
import Pager from './actions_pager';

export default class Actions {
  constructor(containerSelector = '') {
    this.containerSelector = containerSelector;
    this.containerEl = this.containerSelector
      ? document.querySelector(this.containerSelector)
      : undefined;
    this.$contentList = $(`${this.containerSelector} .content_list`);
    this.actionMessage()

    this.loadActions();

    $('.event-filter-link').on('click', (e) => {
      e.preventDefault();
      this.toggleFilter(e.currentTarget);
      this.reloadActions();
    });
  }

  actionMessage(){
      this.msg = this.containerSelector.replace("#","")
      this.msgCapitalize = this.msg[0].toUpperCase() + this.msg.slice(1)
  }
  loadActions() {
    Pager.init({
      limit: 10,
      preload: true,
      prepareData: (data) => data,
      successCallback: () => this.updateTooltips(),
      errorCallback: () =>
        createFlash({
          message: s__(
            `${this.msgCapitalize}|An error occured while retrieving ${this.msg}. Reload the page to try again.`,
          ),
          parent: this.containerEl,
        }),
      container: this.containerSelector,
    });
  }

  updateTooltips() {
    localTimeAgo(document.querySelectorAll(`${this.containerSelector} .content_list .js-timeago`));
  }

  reloadActions() {
    this.$contentList.html('');
    this.loadActions();
  }

  toggleFilter(sender) {
    const $sender = $(sender);
    const filter = $sender.attr('id').split('_')[0];

    $('.event-filter .active').removeClass('active');
    Cookies.set('event_filter', filter);

    $sender.closest('li').toggleClass('active');
  }
}
