export const publishModal = {
    init: function(){
        // 模态框只有在 new 和 edit 情况下才会生效
        if ((!$('[data-page="posts:new"]').length > 0) && (!$('[data-page="posts:edit"]').length > 0)) {
            return;
        }

        // 选择导航栏按钮
        const $modalToggle = $('.post-setting') 

        // 模态框触发按钮
        const $modalTrigger = $('#post-setting-modal')

        $modalToggle.on('click', function(){
            $modalTrigger.click() // 触发
        })
        // 获取模态框中填写的导读信息
        $('#post-lead').on('change', function(){
            var value = $('#post-lead').val().trim()
            $('#post-lead-info').val(value) // 赋值给提交表单中的lead字段
        })

        $('#post-tag').on('change', function(){
            var value = $('#post-tag').val().trim()
            $('#post-tag-info').val(value) // 赋值给提交表单中的lead字段
        })

        // 当前处于编辑页面
        if (!!$('[data-page="posts:edit"]').length > 0) {
            var leadValue = $('#post-lead-info').val() // 获取要编辑的post的导读信息
            $('#post-lead').val(leadValue)

            var tagValue = $('#post-tag-info').val()
            if (tagValue.length != 0) {
                $('#post-tag').val(tagValue)
            }
        }
    }
}

$(document).ready(publishModal.init)