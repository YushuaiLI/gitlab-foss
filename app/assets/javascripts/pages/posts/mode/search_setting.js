export const searchSetting = {
    init: function(){
        $('.selectpicker').selectpicker({
            noneSelectedText: '',
            noneResultsText: '',
            liveSearch: true,
            size:5   //设置select高度，同时显示5个值
        });
    }
}
