import axios from "axios";
import { searchSetting } from './search_setting'

export const modeToggle = (currentUserNamespace) => {
    // 下拉搜索框
    searchSetting.init()

    const groups = $("#collaboration-mode-groups");
	const projects = $("#collaboration-mode-projects");
	const branches = $("#collaboration-mode-branches");
    const files = $("#collaboration-mode-files");
    currentUserNamespace = JSON.parse(currentUserNamespace.currentUserNamespace)
    
    // 建立一个整体的path数组  渐进式获取最终路径 如：
    // 注意 files 只包含/doc目录下的md文件
    // var path = {
    //     "name": "VCO",
    //     "projects": [
    //         {
    //             "name": "feed-collaboration-dup",
    //             "branches": [
    //                 {
    //                     "name": "main",
    //                     "files": [
    //                         "README.md"
    //                     ]
    //                 }
    //             ]
    //         }
            
    //     ]
    // }
	$( async function(){
        var groupsObj = new Array();
        await axios
            .get(`/users/${currentUserNamespace.path}/groups_obj.json`)
            .then(response => {
                // 如果不存在分组
                if (response.data.length == 0){
                    groupsObj.push({"name":"default", "path": currentUserNamespace.path})
                }else{
                    groupsObj.push({"name":"default", "path": currentUserNamespace.path})
                    // 因为分组名和其path可能存在大小写差异
                    response.data.forEach(function(val,index){
                        var obj = {"name": val.name, "path": val.path}
                        groupsObj.push(obj)
                    });
                }
            })
            .catch(e => {
                console.log(e);
                // 如果出错了，直接使用默认
                groupsObj.push({"name":"default", "path": currentUserNamespace.path})
            });
        groupsObj.forEach(function(value,index){
            var groupName = value.name;//分组名
            var groupId = value.path;//分组名
            groups.append("<option value='"+groupId+"'>"+groupName+"</option>");
        });
        $('.selectpicker-groups').selectpicker('refresh');
        $('.selectpicker-groups').selectpicker('render');
	});
	//分组下拉框切换时间,加载项目下拉框值
	groups.change(async function(){
		//清除子级选项
		$("#collaboration-mode-projects option:not(:first)").remove();
		$("#collaboration-mode-branches option:not(:first)").remove();
        $("#collaboration-mode-files option:not(:first)").remove();
        // 重置展示内容
        $("#js-collaboration-content-preview").html("")

        var groups_id = groups.val()
        var projectsObj = new Array();

        if(groups_id === currentUserNamespace.path){
            var api = `/users/${groups_id}/projects_obj.json`
        }else if(groups_id ===''){
            // remove之后重新刷新
            $('.selectpicker-projects').selectpicker('refresh');
            $('.selectpicker-projects').selectpicker('render');
            $('.selectpicker-branches').selectpicker('refresh');
            $('.selectpicker-branches').selectpicker('render');
            $('.selectpicker-files').selectpicker('refresh');
            $('.selectpicker-files').selectpicker('render');
            return
        }else{
            var api = `/groups/${groups_id}/-/projects_obj.json`
        }
        await axios
            .get(api)
            .then(response => {
                response.data.forEach(function(val,index){
                    var obj = {"name": val.name, "path": val.path}
                    projectsObj.push(obj)
                });
            })
            .catch(e => {
                console.log(e);
            });
        projectsObj.forEach(function(value,index){
            var projectName = value.name;//分组名
            var projectId = value.path;//分组名
            projects.append("<option value='"+projectId+"'>"+projectName+"</option>");
		});
        $('.selectpicker-projects').selectpicker('refresh');
        $('.selectpicker-projects').selectpicker('render');
	});
	//项目下拉框切换事件,加载分支下拉框值
	projects.change(async function(){
		$("#collaboration-mode-branches option:not(:first)").remove();
        $("#collaboration-mode-files option:not(:first)").remove();
        // 重置展示内容
        $("#js-collaboration-content-preview").html("")

        var groups_id = groups.val()
        var projects_id = projects.val()
        if(groups_id==="" ||  projects_id===""){
            // remove之后重新刷新
            $('.selectpicker-branches').selectpicker('refresh');
            $('.selectpicker-branches').selectpicker('render');
            $('.selectpicker-files').selectpicker('refresh');
            $('.selectpicker-files').selectpicker('render');
            return
        }
        var branchesObj = new Array();
        await axios
            .get(`/${groups_id}/${projects_id}/refs?sort=updated_desc`)
            .then(response => {
                branchesObj = response.data.Branches
            })
            .catch(e => {
                console.log(e);
            });

            branchesObj.forEach(function(value,index){
			    branches.append("<option value='"+value+"'>"+value+"</option>");
		});
        $('.selectpicker-branches').selectpicker('refresh');
        $('.selectpicker-branches').selectpicker('render');
	});
    //分支下拉框切换事件,加载文件下拉框值
    branches.change(async function(){
        $("#collaboration-mode-files option:not(:first)").remove();
        // 重置展示内容
        $("#js-collaboration-content-preview").html("")

        var groups_id = groups.val()
        var projects_id = projects.val()
        var branches_id = branches.val()

        if(groups_id==="" ||  projects_id==="" || branches_id===""){
            // remove之后重新刷新
            $('.selectpicker-files').selectpicker('refresh');
            $('.selectpicker-files').selectpicker('render');
            return
        }
        var filesObj = new Array();
        await axios
            .get(`/${groups_id}/${projects_id}/-/refs/${branches_id}/logs_tree/doc?format=json&offset=0`)
            .then(response => {
                response.data.forEach(function(val,index){
                    var obj = {"name": val.file_name}
                    filesObj.push(obj)
                });
            })
            .catch(e => {
                console.log(e);
            });

            filesObj.forEach(function(value,index){
                var fileName = value.name;//文件名
			    files.append("<option value='"+fileName+"'>"+fileName+"</option>");
		});
        $('.selectpicker-files').selectpicker('refresh');
        $('.selectpicker-files').selectpicker('render');
	});

    files.change(async function(){
        var groups_id = groups.val()
        var projects_id = projects.val()
        var branches_id = branches.val()
        var file_name = files.val()

        if(groups_id==="" ||  projects_id==="" || branches_id==="" || file_name===""){
            return
        }
        await axios
            .get(`/${groups_id}/${projects_id}/-/blob/${branches_id}/doc/${file_name}?format=json&viewer=rich`)
            .then(response => {
                $("#js-collaboration-content-preview").html(response.data.html)
            })
            .catch(e => {
                console.log(e);
                $("#js-collaboration-content-preview").html('<p style:"color:red">获取文件内容错误</p>')
            });
	});
}