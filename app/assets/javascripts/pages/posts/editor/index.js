//加载medium编辑器插件依赖
import './vendor/jquery.iframe-transport'
import './vendor/jquery.fileupload'

// 加载编辑器
import MediumEditor from 'medium-editor';
import M from './vendor/medium-editor-insert-plugin'

jQuery = M();
$ = M(); 

export const Editor = {
    init: function(){
        /*** Simply return if it's not editor page ***/
        if (!$('[data-page="main-editor"]').length > 0) {
            return;
        }
        // 内容编辑区设置
        var editor = new MediumEditor('.medium-editable',{
            buttonLabels: 'fontawesome',
            toolbar: {
                buttons: [
                    'bold',
                    'italic',
                    'underline',
                    'anchor',
                    'h1',
                    'h2',
                    'quote',
                    'pre',
                    'removeFormat']
            },
            placeholder: {
                text: '分享，让信息更有价值...',
                hideOnClick: true
            }
        });

        // 设置编辑器插件
        $('.medium-editable').mediumInsert({
            editor: editor,
            addons: {
                images: {
                    label: '<i class="fas fa-camera"></i>',
                    deleteScript: "/images/files", // 有待改进
                    deleteMethod: 'DELETE',
                    captionPlaceholder: '图像标题(可选)',
                    fileUploadOptions: { // TODO: figure out how to upload pictures to AS3
                        url: "/images", //
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                        type: 'POST',
                    },
                    styles: {
                        wide: {
                            label: '<i class="fas fa-align-justify"></i>'
                        },
                        left: {
                            label: '<i class="fas fa-align-left"></i>'
                        },
                        right: {
                            label: '<i class="fas fa-align-right"></i>'
                        },
                        grid: {
                            label: '<i class="fas fa-th"></i>'
                        }
                    },
                    actions: {
                        remove: {
                            label: '<i class="fas fa-times"></i>'
                        }
                    }
                },
                embeds: {
                    label: '<i class="fab fa-youtube"></i>',
                    placeholder: "复制视频在B站的网址并按下Enter...",
                    captionPlaceholder: '视频标题(可选)',
                    oembedProxy: null,
                    styles: {
                        wide: {
                            label: '<i class="fas fa-align-justify"></i>'
                        },
                        left: {
                            label: '<i class="fas fa-align-left"></i>'
                        },
                        right: {
                            label: '<i class="fas fa-align-right"></i>'
                        }
                    },
                    actions: {
                        remove: {
                            label: '<i class="fas fa-times"></i>'
                        }
                    }
                }
            }
        });
        // 针对图片上传
        $("#post_picture").change(function(){
            //限制图片大小
            const size_in_megabytes = this.files[0].size / 1024 / 1024
            const file_name = this.files[0].name
            if(size_in_megabytes > 2){
                alert("上传文件大小最大为2MB, 请重新选择文件")
                $('#post_picture').val("")
                return
            }
            // 限制文件类型
            let re = /\.(jpe?g|png)$/i
            if(!re.test(file_name)){
                alert("上传图片允许类型为：jpeg、jpg、png, 请重新选择文件")
                $('#post_picture').val("")
                return 
            }

            Editor.readURL(this);
            // $('#existing-img-previewer').addClass('hidden'); // 编辑情况
            $('.post-picture-upload').addClass('active');
            $('.file-upload-previewer').removeClass('hidden');
            $('.post-picture-upload-cancel').removeClass('hidden');
        });
        // 上传文件清除
        $(".post-picture-upload-cancel").on('click',function(){
            if (!$('#post_picture').length > 0) {
                return;
            }
            $('.post-picture-upload').removeClass('active');
            // 清除文件
            $('#image_preview').attr('src', "") 
            $('.file-upload-previewer').addClass('hidden');
            $('#post_picture').val("")
            // 自身也要隐藏
            $(".post-picture-upload-cancel").addClass("hidden") 
        })

    },
    readURL: function(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#image_preview').attr('src', e.target.result);
          }
    
          reader.readAsDataURL(input.files[0]);
        }
      },
}

$(document).ready(Editor.init)
