import './editor'
import './operations/like'
import './operations/comment'
import './operations/share'
import './desc'

// 视频设置
import './show'
import './publish_modal'

import Vue from 'vue';
import { modeToggle } from './mode'
import App from "./app.vue"

import axios from "axios";

// 写作模式切换
function publishModeToggle(el){
    if (!$('#publish-mode-toggle').length > 0)  {
        return;
    }

    $("#publish-mode-toggle").on("change", function(){
        // 初始状态为false 即为默认模式
        var checked = $('#publish-mode-toggle').is(':checked')

        if(checked){
            // 协作模式开启
            $('#publish-mode-default').addClass('hidden')
            $('#publish-mode-collaboration').removeClass('hidden')
            // 加载数据
            modeToggle(getCurrentUserNamespace(el.dataset))
        }else {
            // 默认模式开启
            $('#publish-mode-collaboration').addClass('hidden')
            $('#publish-mode-default').removeClass('hidden')
        }
    } )
}

// 获取当前用户namespace
function getCurrentUserNamespace(dataset){
    let currentUserNamespace = dataset;

    currentUserNamespace = JSON.parse(JSON.stringify(currentUserNamespace));
    return currentUserNamespace
}

// 创建状态
function publishPost(){
    /*** 对V-blog发布页面进行表单验证 ***/
    // 只要处于 new edit create 任何一个界面就不返回
    if ((!$('[data-page="posts:new"]').length > 0) && (!$('[data-page="posts:edit"]').length > 0) && (!$('[data-page="posts:create"]').length > 0)) {
        return;
    }
    // 获取要验证的元素
    const $title = $('#editor-title')
    const $content_area = $('#content-area')
    const $lead = $('#post-lead')

    // 绑定focus blur 事件, 验证是在失去焦点时触发
    // 标题验证
    $title.on("blur", function(){
        var value = $title.val().trim()
        if (value.length == 0){
            $('#titleErr').text("标题不能为空")
        }else if (value.length > 140){
            $('#titleErr').text("标题不能超过140个字符")
        }
    })
    $title.on('focus', function(){
        if ($('#titleErr').text() != ''){
            $('#titleErr').text('')
        }
    })

    // 内容验证
    $content_area.focusin(function(){
        if ($('#contentErr').text() != ''){
            $('#contentErr').text('')
        }
    })

    $content_area.focusout(function(){
        var value = $('.medium-editor-element p').text().replace('\n','').trim()
        if (value.length == 0){
            $('#contentErr').text("内容不能为空")
        }
    });

    // 导读验证
    $lead.on("blur", function(){
        var value = $lead.val().trim()
        if (value.length == 0){
            $('#leadErr').text("内容不能为空")
        }else if (value.length > 200){
            $('#leadErr').text("内容不能超过200个字符")
        }
    })
    $lead.on('focus', function(){
        if ($('#leadErr').text() != ''){
            $('#leadErr').text('')
        }
    })

    /*** Form submit ***/
    $('[data-behavior="publish-button"]').on('click', async function() {
        // 默认模式  以及编辑状态 或是提交出错
        if ((!$('#publish-mode-toggle').length > 0) && (!$('[data-page="posts:edit"]').length > 0) && (!$('[data-page="posts:create"]').length > 0) ) {
            return;
        }
        // 根据切换按钮状态选择发布模式
        var checked = $('#publish-mode-toggle').is(':checked')
        // 默认模式  以及编辑状态 或是提交出错
        if (!checked || ($('[data-page="posts:edit"]').length > 0) || ($('[data-page="posts:create"]').length > 0)) {
            // 触发验证
            $title.focus() // 代码触发
            $title.blur() //  触发了blur

            $content_area.focusin() // 代码触发
            $content_area.focusout() //  触发了blur

            $lead.focus() // 代码触发
            $lead.blur() //  触发了blur

            // 必须错误清空了才能提交
            if ($('#titleErr').text() == '' && $('#contentErr').text() == '') {
                $('.editor-form').submit();
            }
        }else{
            let re =  /(.+)\.md$/
            var title = $("#collaboration-mode-files").val()
            if(!title){
                alert("请选择要分享的V-blog")
                return
            }
            title = title.replace(re, '$1')
            var content = $("#js-collaboration-content-preview").html()
            if(!content){
                alert("要分享的V-blog内容为空，请进行补充")
                return
            }
           await axios
                    .post(`/posts`, {
                        title: title,
                        content: content,
                        lead: $('#post-lead').val(),
                        tag_list: $('#post-tag').val(),
                        xhr: true
                    })
                    .then(response => {
                        window.location = "/posts/" + response.data.id
                    })
                    .catch(error => {
                        console.error(error);
                    });
        }
        
    });

} 
$(document).ready( function(){
    // 新post发布
    const el = document.getElementById('js-publish-collaboration');
    if (!el &&  (!$('[data-page="posts:edit"]').length > 0) && (!$('[data-page="posts:create"]').length > 0)) {
        return false;
    }

    // 模式切换
    publishModeToggle(el)
    // 内容发布
    publishPost()

    var vm = new Vue({
        el,
        render: h => h(App)
    })

} );
