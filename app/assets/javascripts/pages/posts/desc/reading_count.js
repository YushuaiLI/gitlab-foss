import axios from "axios";

export const readingCount = {
    init: function(){
        /*** Simply return if it's not post show page ***/
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }
     
        // 创建点赞
        if ($('#reaidng-count').length > 0){
            var $el = $('#reaidng-count .count')
            var path = window.location.pathname + '/reading_count'
            axios
                .post(path)
                .then(response => {
                    $el.text(response.data.count)
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }
}

$(document).ready(readingCount.init)