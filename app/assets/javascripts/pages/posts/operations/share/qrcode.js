// 使用CMD规范引入 qrcode
require('jquery.qrcode')  

export const Code = {
    init: function(){
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }

        document.getElementById('share').onclick = function(event){
            //取消冒泡
            let oevent = event || window.event
            if (document.all) {
                oevent.cancelBubble = true
            } else {
                oevent.stopPropagation()
            }
            if (document.getElementById('wechat-code').style.display === 'none' || document.getElementById('wechat-code').style.display === '') {
                document.getElementById('wechat-code').style.display = 'block'
            } else {
                document.getElementById('wechat-code').style.display = 'none'
            }
        }

        document.onclick = function () {
            document.getElementById('wechat-code').style.display = 'none'
        }
        document.getElementById('wechat-code').onclick = function (event) {
            let oevent = event || window.event
            oevent.stopPropagation()
        }
        //  生成二维码
        $('#output').qrcode({
            render : 'canvas',
            width : 100,
            height: 100,
            text : window.location.href
        });

        $('.close').click(function(){
            document.getElementById('wechat-code').style.display = 'none'
        })

    }
}

$(document).ready(Code.init)