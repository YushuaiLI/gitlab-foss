import wx from 'js-api-wx'
import axios from "axios";
// 导入二维码
import './qrcode'

export const Share = {
    init: function(){
        /*** Simply return if it's not post show page ***/
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }
        
        // 已经登录
        if ($('#share button').length > 0) {
          // 获取当前url
          var url = window.location.href

          // 从后端获取签名数据
          axios.get('/signature', {
                params: { url: url}
              })
              .then(function(response){
                var data = response.data
                // 参考https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#1
                //  ===================微信接口相关========================
                
                // 通过config接口注入权限验证配置
                wx.config({
                  debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                  appId: data.appid, // 必填，公众号的唯一标识
                  timestamp: data.timestamp, // 必填，生成签名的时间戳
                  nonceStr: data.noncestr, // 必填，生成签名的随机串
                  signature: data.signature,// 必填，签名
                  jsApiList: [
                    "updateAppMessageShareData",
                    "updateTimelineShareData"
                  ] // 必填，需要使用的JS接口列表
                });
              })
              .catch(function (error) {
                console.log(error);
              })
              
              // 通过ready接口处理成功验证
          
              // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，
              // config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，
              // 则须把相关接口放在ready函数中调用来确保正确执行。
              // 对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
              wx.ready(function () {   //需在用户可能点击分享按钮前就先调用
                wx.updateAppMessageShareData({ 
                  title: $('#article .title').text().replace(/\n|\r/g, ""), // 分享标题
                  desc: $('#article .lead p i').text().replace(/\n|\r/g, ""), // 分享描述
                  link: window.location.href, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                  imgUrl: $('#article .cover-picture img').length ? $('#article .cover-picture img')[0].src : `${window.location.origin}/assets/share.png`, // 分享图标
                  success: function () {
                    // 设置成功
                    console.log("分享成功")
                  },
                  fail: function (res) {
                    console.log("出现错误")
                  },
                  complete: function(){
                    console.log("完成")
                  },
                  trigger: function(){
                    console.log("用户点击")
                  }, 
                  cancel: function(){
                    console.log("用户取消")
                  }
                })
                wx.updateTimelineShareData({ 
                  title: $('#article .title').text().replace(/\n|\r/g, ""), // 分享标题
                  desc: $('#article .lead p i').text().replace(/\n|\r/g, ""), // 分享描述
                  link: window.location.href, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                  imgUrl: $('#article .cover-picture img').length ? $('#article .cover-picture img')[0].src : `${window.location.origin}/assets/share.png`, // 分享图标
                  success: function () {
                    // 设置成功
                    console.log("分享成功")
                  },
                  fail: function (res) {
                    console.log("出现错误")
                  },
                  complete: function(){
                    console.log("完成")
                  },
                  trigger: function(){
                    console.log("用户点击")
                  }, 
                  cancel: function(){
                    console.log("用户取消")
                  }
                })
              })

              // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，
              // 也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
              wx.error(function(res){
                console.log(res);
              }) 
   
      }
  }      
}

$(document).ready(Share.init)