import Actions from '~/actions';

export default class LoadComments {
    constructor() {
        new Actions('#comments_list');
    }
}