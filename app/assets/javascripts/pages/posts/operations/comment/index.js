// 加载编辑器
import MediumEditor from 'medium-editor';

import LoadComments from './load_comments';

export const Comment = {
    init: function(){
        /*** Simply return if it's not post show page ***/
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }

        // 未登录
        if ($('#comment-no-sign button').length > 0) {
            $('#comment-no-sign button').on('click', function(){
                window.location = "/users/sign_in?redirect_to_referer=yes"
            })
        }

        const $commentButton = $('#comment')
        const $commentModal = $('#comment-modal')
        var loadComments = false
        $commentButton.on('click', function(){
            if(!loadComments){
                new LoadComments() // 加载评论
                loadComments = true  // 只加载一次
            }
            $commentModal.click()
        })
        
        // 评论编辑器设置
        var editorComment = new MediumEditor('.medium-editable-comment',{
            buttonLabels: 'fontawesome',
            toolbar: {
                buttons: [
                    'bold',
                    'italic',
                    'underline',
                    'anchor',
                    'pre']
            },
            placeholder: {
                text: '分享，让信息更有价值...',
                hideOnClick: true
            }
        });


        // 评论区输入文字判断
        const $comment_area = $('#comment-area')

        $comment_area.focusin(function(){
            if ($('#commentErr').text() != ''){
                $('#commentErr').text('')
            }
            var value = $('.medium-editor-element p').text().replace('\n','').trim()
            if (value.length > 0){
                $('#publish-comment button').removeClass('disabled')
            }
        })

        $comment_area.focusout(function(){
            var value = $('.medium-editor-element p').text().replace('\n','').trim()
            if (value.length > 0){
                $('#publish-comment button').removeClass('disabled')
            }
            if (value.length == 0){
                $('#commentErr').text("内容不能为空")
                $('#publish-comment button').addClass('disabled')
            } else if(value.length > 500){
                $('#commentErr').text("评论不能超过140个字符")
            }
        });
        // 评论提交
        $('#publish-comment').on('click', function(){
            $comment_area.focusin() // 代码触发
            $comment_area.focusout() //  触发了blur

            if ($('#commentErr').text() == '') {
                $('.comment-form').submit();
            }
        })
    }
}

$(document).ready(Comment.init)