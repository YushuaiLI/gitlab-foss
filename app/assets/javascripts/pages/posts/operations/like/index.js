import axios from "axios";

export const Like = {
    init: function(){
        /*** Simply return if it's not post show page ***/
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }

        // 未登录
        if ($('#clap-no-sign button').length > 0) {
            $('#clap-no-sign button').on('click', function(){
                window.location = "/users/sign_in?redirect_to_referer=yes"
            })
        }
     
        // 创建点赞
        if ($('#clap').length > 0){
            var $el = $('#clap button')
            var path = $el.data()['path']
            var post_id = $el.data()['postId']
            var user_id = $el.data()['userId']
            var $count = $('#clap-count p')
            var clickCount = 0
    
            $('#clap').on('click', async function(){
                if (clickCount == 0){
                    // 点赞
                    clickCount = 1
                    await axios
                        .post(path, {
                            post_id: post_id,
                        })
                        .then(response => {
                            $count.text(response.data.count)
                            $('#clap button svg').addClass('clapped')
                        })
                        .catch(error => {
                            console.error(error);
                        });
                }else if(clickCount==1){
                    // 取消点赞
                    clickCount = 0
                    var param={post_id: post_id}
                    await axios
                        .delete("/destroy/likes", {params: param})
                        .then(response => {
                            $count.text(response.data.count)
                            $('#clap button svg').removeClass('clapped')
                        })
                        .catch(error => {
                            console.error(error);
                        });
                    }
            })
        }
    }
}

$(document).ready(Like.init)