export const Show = {
    init: function(){
        /*** Simply return if it's not post show page ***/
        if (!$('[data-page="posts:show"]').length > 0) {
            return;
        }

        $('#post-insert-video').removeClass('medium-insert-embeds')
        $('.medium-insert-buttons').remove()

        // 设置滚动事件
        window.addEventListener('scroll', () => {
            if (window.pageYOffset >= 100) {
                $("#post-float").removeClass("hidden");
            }
            else {
                $("#post-float").addClass("hidden");
            }
        });

    }
}

$(document).ready(Show.init)