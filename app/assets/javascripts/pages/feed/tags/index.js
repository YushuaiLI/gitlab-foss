import axios from "axios";
import createFlash from '~/flash';
import { s__ } from '~/locale';

export const LoadTags = {
    init: function(){
        if (!$('#site-intro').length > 0) {
            return;
        }
        //  获取url
        const url = $("#tags_list .tag_content_list").data('href')
        axios
            .get(url)
            .then(({ data }) => {
                LoadTags.append(data.count, LoadTags.prepareData(data.html));
            })
            .catch((err) => LoadTags.errorCallback(err))
    },
    // 错误回调
    errorCallback(err){
        createFlash({
            message: s__(
              `${err}`,
            ),
            parent: document.querySelector("#popular_list")
          })
    },
    // 数据准备
    prepareData: (data) => data,
    // 添加在页面中
    append(count, html) {
        $("#tags_list .tag_content_list").append(html);
      }
}


$(document).ready(LoadTags.init)