/*
    这里用于设置信息流首页的行为
*/
import $ from 'jquery';
import LoadFeeds from './load';
import { LoadPopular } from './popular'
import { LoadTags } from './tags'

function feedNavSet() {
    if (!$('#site-intro').length > 0) {
        return;
    }
    // 从开始进入首页就开始加载
    let loadFeedFlag = false 
    if(!loadFeedFlag){
        new LoadFeeds()
        loadFeedFlag = true
    }
    
    $('#hot-refresh').on('click',function(){
        $("#popular_list .hot_content_list").empty();
        LoadPopular.init()
    })

    $('#tags-refresh').on('click',function(){
        $("#tags_list .tag_content_list").empty();
        LoadTags.init()
    })

    window.addEventListener('scroll', () => {
        if (window.pageYOffset >= $('#site-intro').get(0).offsetHeight) {
            $("#feed-nav-set").addClass('nav-sticky');
            const topic_h = 401 + 286.375 + 56;
            if (window.pageYOffset >= topic_h) {
                $("#feed-tags").addClass("tag-sticky");
            }
            else {
                $("#feed-tags").removeClass("tag-sticky");
            } 
        }
        else {
            $("#feed-nav-set").removeClass("nav-sticky");
        }
    });
}

feedNavSet()