import Actions from '~/actions';

export default class LoadFeeds {
    constructor() {
        new Actions('#feeds_list');
    }
}