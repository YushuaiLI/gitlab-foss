import axios from "axios";
import createFlash from '~/flash';
import { s__ } from '~/locale';

export const LoadPopular = {
    init: function(){
        if (!$('#site-intro').length > 0) {
            return;
        }
        //  获取url
        const url = $("#popular_list .hot_content_list").data('href')
        axios
            .get(url)
            .then(({ data }) => {
                LoadPopular.append(data.count, LoadPopular.prepareData(data.html));
            })
            .catch((err) => LoadPopular.errorCallback(err))
    },
    // 错误回调
    errorCallback(err){
        createFlash({
            message: s__(
              `${err}`,
            ),
            parent: document.querySelector("#popular_list")
          })
    },
    // 数据准备
    prepareData: (data) => data,
    // 添加在页面中
    append(count, html) {
        $("#popular_list .hot_content_list").append(html);
      }
}


$(document).ready(LoadPopular.init)