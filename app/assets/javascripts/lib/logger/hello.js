import { s__, sprintf } from '~/locale';

const HANDSHAKE = String.fromCodePoint(0x1f91d);
const MAG = String.fromCodePoint(0x1f50e);
const ROCKET = String.fromCodePoint(0x1f680);

export const logHello = () => {
  // eslint-disable-next-line no-console
  console.log(
    `%c${s__('HelloMessage|欢迎使用VCO!')}%c

${s__(
  'HelloMessage|如果您在使用的过程中有什么问题或是改进，可以前往以下地址：',
)}

${sprintf(s__('HelloMessage|%{handshake_emoji} Contribute to VCO: %{contribute_link}'), {
  handshake_emoji: `${HANDSHAKE}`,
  contribute_link: '贡献链接',
})}
${sprintf(s__('HelloMessage|%{magnifier_emoji} Create a new VCO issue: %{new_issue_link}'), {
  magnifier_emoji: `${MAG}`,
  new_issue_link: '问题或改进链接',
})}
${
  window.gon?.dot_com
    ? `${sprintf(
        s__(
          'HelloMessage|%{rocket_emoji} We like your curiosity! Help us improve GitLab by joining the team: %{jobs_page_link}',
        ),
        { rocket_emoji: `${ROCKET}`, jobs_page_link: 'https://about.gitlab.com/jobs/' },
      )}`
    : ''
}`,
    `padding-top: 0.5em; font-size: 2em;`,
    'padding-bottom: 0.5em;',
  );
};
