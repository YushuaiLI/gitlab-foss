# frozen_string_literal: true

class CommentsController < ApplicationController

  before_action :find_post
  def index
    load_comments
    present_comments(@comment_items)
  end

  def create
    @comment = @post.comments.build(content:comment_params[:content], user_id: current_user.id)
    if @comment.save
      flash[:success] = "评论发表成功"
      redirect_to post_path(@post)
    else
      render json:{failed:"评论创建失败，请检查输入是否正确!"}, status: 422
    end
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    flash[:success] = "评论删除成功"

    redirect_to @post
  end

  def present_comments(comment_items)
    respond_to do |format|
      format.json do
        pager_json("posts/comments/_comment_items", comment_items.count, comment_items: comment_items)
      end
    end
  end

  def load_comments
    @comment_items = @post.comments
      .limit(params[:limit])
      .offset(params[:offset])
  end

  private
    def find_post        
      @post = Post.find(params[:post_id])
    end

    def comment_params
      params.require(:comment).permit(:content)
    end
end
CommentsController.prepend_mod_with('CommentsController')