# frozen_string_literal: true

class SignatureController < ApplicationController
    # 生成签名的所需要的元素
    # 1. 随机字符串
    # 2. jsapi_ticket
    # 3. 当前时间戳
    # 4. 要分享的url
    # 备注 jsapi_ticket 是通过access_token获取的，而access_token是由微信公众号的AppID和AppSecret请求获取得到的
    # jsapi_ticket 和 access_token 每天有请求次数限制，需要缓存，同时也有过期时间
    include SignatureHelper
    skip_before_action :authenticate_user!
    before_action :instance_client

    def signature
        # 获取要分享的url
        url = params[:url]
        # 随机字符串
        noncestr = create_noncestr

        jsapi_ticket ||= @client.get_jsapi_ticket() # ||= 有缓存效果，如果服务器不停止运行，变量值不会释放, 即不会发起请求
        
        # 已经到了要刷新的时间
        if @client.expire_time < Time.now.to_i
            @client.expire_time = nil # expire_time重新计算
            jsapi_ticket = @client.get_jsapi_ticket() # 重新发起请求
        end

        if jsapi_ticket.nil?
            render json: {error: "当前文章无法分享,请稍后再试"}, status: 422
        else
            timestamp = Time.now.to_i
            jsapi_params = {
                noncestr: noncestr,
                jsapi_ticket: jsapi_ticket,
                timestamp: timestamp,
                url: url
            }
            signature = sign_param(jsapi_params)

            render json: {
                appid: @client.appID,
                timestamp: timestamp,
                noncestr: noncestr,
                signature: signature 
            }, status: 200
        end
    end
        
    private
        def instance_client
            #  如果已经实例化，就不需要在进行实例化
            @client ||= WechatHttpClient.new
        end

end

SignatureController.prepend_mod_with('SignatureController')