class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :edit, :update, :destroy]

  # GET /images
  def index
    @images = Image.all
  end

  # GET /images/1
  def show
  end

  # GET /images/new
  def new
    @image = Image.new
  end

  # GET /images/1/edit
  def edit
  end

  # POST /images
  def create
    file = params[:file]
    @image = Image.new(file:file)

    respond_to do |format|
      if @image.save
        format.html { redirect_to @image, notice: 'Image was successfully created.' }
        format.json do
          render json: {
            files:
              [
                {
                  url: @image.file.url,
                  name: @image.file_identifier,
                  type: "image/jpeg",
                  size: @image.file.size,
                }
              ]
          }
        end
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /images/1
  def update
    if @image.update(image_params)
      redirect_to @image, notice: 'Image was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /images/1
  def destroy
    @image.destroy
    redirect_to images_url, notice: 'Image was successfully destroyed.'
  end

  # DELETE images/files  TODO: 有待改进
  def destroy_files
    path = params[:file]
    # 获取路径中的id
    re = /.*\/file\/(\d+)\/.*/ 
    id = re.match(path)[1].to_i
    
    @image = Image.find(id)
    if @image.present?
      @image.destroy
      render json:{info:"删除成功"}, status:200
    else
      render json:{error:"文件已经删除"}, status:200
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def image_params
      params.require(:image).permit(:file)
    end
end

ImagesController.prepend_mod_with('ImagesController')