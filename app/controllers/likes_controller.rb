# frozen_string_literal: true

class LikesController < ApplicationController
    include LikesHelper
    before_action :find_post

    def create
        if already_liked?(current_user, @post)
            render json:{alert: "您已经对当前V-blog点赞"}, status: 200
        else
            @like = @post.likes.build(user_id: current_user.id)
            if @like.save
                render json:{count: @post.likes.count}, status: 200
            else
                render json:{failed: "创建点赞失败，请检查当前点赞条件是否允许!"}, status: 422
            end
        end
    end

    def destroy_likes
        @like = Like.find_by(user_id: current_user.id, post_id: @post.id)
        @like.destroy
        render json:{count: @post.likes.count}, status: 200
      end

    private
        def find_post        
            @post = Post.find(params[:post_id])
        end
end

LikesController.prepend_mod_with('LikesController')
