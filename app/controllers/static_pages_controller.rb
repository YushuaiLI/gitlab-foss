# frozen_string_literal: true

class StaticPagesController < ApplicationController
  layout "home_layout"  # 指定信息流布局
  
  # 登录前行为 首页信息流跳过登录
  skip_before_action :authenticate_user!

  def home
  end
  
  # 平台介绍
  def about
  end

  # 目前展示所有信息流 没有推荐机制
  def feed
    load_feeds
    present_feeds(@feed_items)
  end

  def popular
    load_popular
    present_popular(@popular_items)
  end

  def popular_tags
    load_tags
    present_tags(@tags)
  end

  def present_feeds(feed_items)
    respond_to do |format|
      format.json do
        pager_json("static_pages/feeds/_feed_items", feed_items.count, feed_items: feed_items)
      end
    end
  end

  def present_popular(popular_items)
    respond_to do |format|
      format.json do
        pager_json("static_pages/popular/_popular_items", popular_items.count, popular_items: popular_items)
      end
    end
  end

  def present_tags(tags)
    respond_to do |format|
      format.json do
        pager_json("static_pages/tags/_tags", tags.count, tags: tags)
      end
    end
  end

  def load_feeds
    @feed_items = Post.order(created_at: :DESC)
      .limit(params[:limit])
      .offset(params[:offset])
  end

  def load_popular
    ids = []
    sql = 'SELECT post_id, count(post_id) AS count FROM "likes" GROUP BY "likes"."post_id" ORDER BY count desc LIMIT 6'
    result = ActiveRecord::Base.connection.execute(sql)
    result.each do |value| 
      ids.append(value["post_id"])
    end

    # 转换为Post对象
    @popular_items = Post.find(ids)
  end

  def load_tags
    ids = []
    sql = 'SELECT post_tag_id, count(post_tag_id) AS count FROM "post_taggings" GROUP BY "post_taggings"."post_tag_id" ORDER BY count desc LIMIT 20'
    result = ActiveRecord::Base.connection.execute(sql)
    result.each do |value| 
      ids.append(value["post_tag_id"])
    end

    # 转换为PostTag对象
    @tags = PostTag.find(ids)
  end

end

StaticPagesController.prepend_mod_with('StaticPagesController')