# frozen_string_literal: true

class PostsController < ApplicationController
    include SearchHelper
    include InternalRedirect
    layout "home_layout"
    skip_before_action :authenticate_user!, only: [:index, :show]
    before_action :correct_user, only: [:edit, :update, :destroy] # 避免水平越权

    def index
        @posts = Post.all
    end

    def new
        @post = Post.new
    end

    def show
        @post = Post.find(params[:id])
    end

    def create
        # 判断是否为异步请求  妥协方案，由客户端提交参数，安全性待进一步改进
        xhr = params[:xhr]
        # 重新组装参数
        redirect_path = current_user.namespace.path # 从协作中心发布后，方便进行跳转
        if xhr
            title = params[:title]
            content = params[:content]
            lead = params[:lead]
            tag_list = params[:tag_list]
            @post = current_user.posts.build(title: title, content: content, lead: lead, tag_list: tag_list, redirect_path:redirect_path)
        else
            
            @post = current_user.posts.build(post_params)
        end
        if @post.save
            if xhr
                render json: @post
            else
                flash[:success] = "V-blog发布成功"
                redirect_to @post
            end
        else
            render :new  
        end
    end

    def edit
        @post = Post.find(params[:id])
    end

    def update
        @post = Post.find(params[:id])

        if @post.update(post_params)
            flash[:success] = "V-blog更新成功"
            redirect_to @post
        else
            render 'edit'
        end
    end

    def destroy
        @post = current_user.posts.find(params[:id])
        @post.destroy
        flash[:success] = "V-blog删除成功"

        redirect_path = referer_path(request) || user_posts_path(current_user)

        redirect_to redirect_path
    end

    # post 
    def reading_count
        post = Post.find(params[:id])
        count = post.reading_count + 1
        # 更新
        if post.update({reading_count: count})
            if count > 1000
                count = "1k+"
            end 
            render json: {count: count.to_s}, status:200 
        else
            render json: {error: "出入参数非法"}, status:422
        end
    end

    private

        def post_params
            params.require(:post).permit(:title, :content, :picture, :lead, :tag_list)
        end

        def correct_user
            @post = current_user.posts.find_by(id: params[:id])  # 如果删除其他用户的微博会返回nil
            redirect_path = referer_path(request) || user_posts_path(current_user)
            if @post.nil? 
                flash[:alert] = "非文章作者, 当前操作不允许"
            end
            redirect_to redirect_path if @post.nil? 
        end

end

PostsController.prepend_mod_with('PostsController')