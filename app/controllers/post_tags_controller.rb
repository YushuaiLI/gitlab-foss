# frozen_string_literal: true

class PostTagsController < ApplicationController
    include PageLimiter

    skip_before_action :authenticate_user!, only: [:index, :show]

    def index
        load_tags
    end

    def show
        load_tag

        return render_404 unless @tag

        @posts = load_posts
        
    end
    private
        def load_posts_count
            @count = @tag.posts.count 
        end

        def load_posts                             
            posts = PostTags::PostsFinder.new(@tag, params: params.permit(:search)).execute.page(params[:page]).without_count       
        end

        def load_tags
            @tags = Posts::TagsFinder.new(params: params.permit(:search)).execute.page(params[:page]).without_count  
        end

        def load_tag
            @tag = PostTag.find(params[:id])
        end

end

PostsController.prepend_mod_with('PostsController')