# frozen_string_literal: true

class PostImagePolicy < BasePolicy
  
    rule { default }.enable :read_image
end
    
PostImagePolicy.prepend_mod_with('PostImagePolicy')  