# frozen_string_literal: true

class PostPolicy < BasePolicy
  
  rule { default }.enable :read_post
end
  
PostPolicy.prepend_mod_with('PostPolicy')  