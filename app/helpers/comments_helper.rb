module CommentsHelper


    def author_avatar_comment(comment, options = {})
        user_avatar(options.merge({
        user: comment.user,
        user_name: comment.user.name,
        user_email: comment.user.email,
        css_class: 'd-none d-sm-inline-block'
        }))
    end
    def icon_for_profile_comment(comment)
        content_tag :div, class: 'system-note-image user-avatar' do
            author_avatar_comment(comment, size: 32)
        end
    end

    def link_to_comment_user(comment, self_added: false)
        user = comment.user
    
        if user
          name = self_added ? 'You' : user.name
          link_to name, user_path(user.username), title: name
        else
          escape_once(comment.user.name)
        end
    end
    def comment_user_info(comment)
        content_tag(:div, class: "comment-user-info") do
          concat content_tag(:span, link_to_comment_user(comment), class: "user-name")
          concat "&nbsp;".html_safe
          concat content_tag(:span, comment.user.to_reference, class: "username")
        end
    end
end
