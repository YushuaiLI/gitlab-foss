module SignatureHelper
    # 生成随机字符串
    def create_noncestr
        len = 30
        chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
        newpass = ""
        1.upto(len) {|i| newpass << chars[rand(chars.size-1)]}
        newpass
    end

    # 微信请求的客户端
    class WechatHttpClient
        include HTTParty
        base_uri 'https://api.weixin.qq.com/cgi-bin'
        attr_accessor :appID, :expire_time  # 提供外部调用
      
        def initialize()
            # 正式公众号
            @appID = ENV['appID']
            @appSecret = ENV['appSecret']
        end
      
        def get_access_token
            options = { query: { grant_type: "client_credential", 
                                appid: @appID,
                                secret: @appSecret } }
            res = self.class.get("/token", options).parsed_response
            if res["errcode"].nil?
                res["access_token"]
            else
                nil
            end
        end
      
        def get_jsapi_ticket
            access_token = self.get_access_token() # 这个值没有必要缓存
            if access_token.nil?
                nil
            else
                options = { query: { access_token: access_token, 
                                        type: "jsapi"} }
                res = self.class.get("/ticket/getticket", options).parsed_response
                if res["errcode"].eql?(0)
                    @expire_time ||= Time.now.to_i + 7000 # 存储将来过期的时间  官方过期时间为7200 这里提前刷新
                    res["ticket"]
                else
                    nil
                end
            end
        end
    end
    #  生成签名
    def sign_param(param)
        # 对所有待签名参数按照字段名的ASCII 码从小到大排序（字典序）
        param = param.sort_by{ |a,b| a.to_s}.to_h
        
        format = []
        param.each_with_index do |value|
            format << "#{value[0]}=#{value[1]}"
        end
        format = format.join('&') 
    
        Digest::SHA1.hexdigest(format)
    end
end
