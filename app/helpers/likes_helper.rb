module LikesHelper
    def already_liked?(user, post)
        !Like.find_by(user_id: user.id, post_id: post.id).nil?
    end
end
