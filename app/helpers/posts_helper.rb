module PostsHelper
    # 安全渲染html内容
    def remove_javascript(html)
        html.gsub(/<script.*?>/i, "")
            .gsub(/<\/script>/i, "")
            .gsub(/javascript:/i, "")
            .gsub(/on[\w]+=/i, "")
    end
    # 估计阅读时间
    def reading_time(content)
        words_per_minute = 150
        # 估计时间，只统计p标签下的内容
        fragment = Nokogiri::HTML.fragment(content).search('p')
        text = ""
        fragment.each do |t|
            text += t.text
        end
        time = text.length / words_per_minute

        time = time.eql?(0) ? 1 : time
    end

    def author_avatar_post(post, options = {})
        user_avatar(options.merge({
        user: post.user,
        user_name: post.user.name,
        user_email: post.user.email,
        css_class: 'd-none d-sm-inline-block'
        }))
    end
    def icon_for_profile_post(post)
        content_tag :div, class: 'system-note-image user-avatar' do
            author_avatar_post(post, size: 32)
        end
    end
    def link_to_post_user(post, self_added: false)
        user = post.user
    
        if user
          name = self_added ? 'You' : user.name
          link_to name, user_path(user.username), title: name
        else
          escape_once(post.user.name)
        end
    end
    def post_user_info(post)
        content_tag(:div, class: "post-user-info") do
          concat content_tag(:span, link_to_post_user(post), class: "user-name")
          concat "&nbsp;".html_safe
          concat content_tag(:span, post.user.to_reference, class: "username")
        end
    end
end
