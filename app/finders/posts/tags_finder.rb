# frozen_string_literal: true

# Used to filter posts tags by a set of params
#
# Arguments:
#   params:
#     search: string
module Posts
    class TagsFinder
      def initialize(params: {})
        @params = params
      end
  
      def execute
        #  按照ASCII 的顺序进行排列
        tags = PostTag.all.order(:name)
        by_search(tags)
      end
  
      private
  
      attr_reader :current_user, :params
  
      def by_search(tags)
        return tags unless params[:search].present?
  
        PostTag.search(params[:search]).order(:name)
      end
    end
  end
  