# frozen_string_literal: true

# Used to filter posts for tag by a set of params
#
# Arguments:
#   params:
#     search: string
module PostTags
    class PostsFinder
      def initialize(tag, params: {})
        @tag = tag
        @params = params        
      end
  
      def execute        
        posts = @tag.posts.order(created_at: :DESC)
        by_search(posts)
      end
  
      private
  
      attr_reader :current_user, :params
  
      def by_search(posts)
        return posts unless params[:search].present?
  
        posts.search(params[:search])
      end
    end
  end
  