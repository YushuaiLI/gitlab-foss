# frozen_string_literal: true

class PostTag < ApplicationRecord
    include Gitlab::SQL::Pattern

    has_many :post_taggings, dependent: :destroy
    has_many :posts, through: :post_taggings

    extend FriendlyId
    friendly_id :name

    def to_s
        name
    end

    # search  class method
    # 全局搜索
    def PostTag.search(query)
        fuzzy_search(query, [:name])
    end
end

PostTag.prepend_mod_with('PostTag')