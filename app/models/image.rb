# frozen_string_literal: true

# 用于文件上传
require 'carrierwave/orm/activerecord'

class Image < ApplicationRecord
    extend Gitlab::ConfigHelper

    include Gitlab::ConfigHelper
    include PostImage

    validates :file,  presence: true
end

Image.prepend_mod_with('Image')