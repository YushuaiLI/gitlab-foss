# frozen_string_literal: true

class Like < ApplicationRecord
  belongs_to :post
  belongs_to :user

  validates :user_id,  presence: true
  validates :post_id,  presence: true

  # VCO 热搜 展示点赞数最多的6个  count为0 表示并为创建 有待改进
  # scope :popular, -> { select('post_id, count(post_id) as count').group(:post_id).order('count desc').limit(6) }
end
Like.prepend_mod_with('Like')
