# frozen_string_literal: true

class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  # 展示顺序  降序
  default_scope  lambda { order(created_at: :DESC)}
  
  validates :content,  presence: true, length: { maximum: 500}
  validates :user_id,  presence: true
  validates :post_id,  presence: true
end
Comment.prepend_mod_with('Comment')