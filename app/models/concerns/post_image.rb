# frozen_string_literal: true

module PostImage
    extend ActiveSupport::Concern
  
    # USER_AVATAR_SIZES = [16, 20, 23, 24, 26, 32, 36, 38, 40, 48, 60, 64, 90, 96, 120, 160].freeze
    # PROJECT_AVATAR_SIZES = [15, 40, 48, 64, 88].freeze
    # GROUP_AVATAR_SIZES = [15, 37, 38, 39, 40, 64, 96].freeze
  
    # ALLOWED_IMAGE_SCALER_WIDTHS = (USER_AVATAR_SIZES | PROJECT_AVATAR_SIZES | GROUP_AVATAR_SIZES).freeze
  
    # This value must not be bigger than then: https://gitlab.com/gitlab-org/gitlab/-/blob/master/workhorse/config.toml.example#L20
    #
    # https://docs.gitlab.com/ee/development/image_scaling.html
    MAXIMUM_FILE_SIZE = 5.megabytes.to_i
  
    included do
      prepend ShadowMethods
      include ObjectStorage::BackgroundMove
      include Gitlab::Utils::StrongMemoize
      include ApplicationHelper
  
      validates :file, file_size: { maximum: MAXIMUM_FILE_SIZE }, if: :file_changed?
  
      mount_uploader :file, ImageUploader
  
    #   after_initialize :add_file_to_batch
    #   after_commit :clear_file_caches
    end
  
    module ShadowMethods
      def file_url(**args)
        # We use file_path instead of overriding file_url because of carrierwave.
        # See https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/11001/diffs#note_28659864
  
        file_path(only_path: args.fetch(:only_path, true), size: args[:size]) || super
      end
  
      def retrieve_upload(identifier, paths)
        upload = retrieve_upload_from_batch(identifier)
  
        # This fallback is needed when deleting an upload, because we may have
        # already been removed from the DB. We have to check an explicit `#nil?`
        # because it's a BatchLoader instance.
        upload = super if upload.nil?
  
        upload
      end
    end
  
    class_methods do
      def bot_file(image:)
        Rails.root.join('lib', 'assets', 'images', 'bot_files', image).open
      end
    end
  
    def file_type
      unless self.file.image?
        errors.add :file, "file format is not supported. Please try one of the following supported formats: #{ImageUploader::SAFE_IMAGE_EXT.join(', ')}"
      end
    end
  
    def file_path(only_path: true, size: nil)
      unless self.try(:id)
        return uncached_file_path(only_path: only_path, size: size)
      end
  
      # Cache this file path only within the request because files in
      # object storage may be generated with time-limited, signed URLs.
      key = "#{self.class.name}:#{self.id}:#{only_path}:#{size}"
      Gitlab::SafeRequestStore[key] ||= uncached_file_path(only_path: only_path, size: size)
    end
  
    def uncached_file_path(only_path: true, size: nil)
      return unless self.try(:file).present?
  
      asset_host = ActionController::Base.asset_host
      use_asset_host = asset_host.present?
      use_authentication = respond_to?(:public?) && !public?
      query_params = size&.nonzero? ? "?width=#{size}" : ""
  
      # files for private and internal groups and projects require authentication to be viewed,
      # which means they can only be served by Rails, on the regular GitLab host.
      # If an asset host is configured, we need to return the fully qualified URL
      # instead of only the file path, so that Rails doesn't prefix it with the asset host.
      if use_asset_host && use_authentication
        use_asset_host = false
        only_path = false
      end
  
      url_base = []
  
      if use_asset_host
        url_base << asset_host unless only_path
      else
        url_base << gitlab_config.base_url unless only_path
        url_base << gitlab_config.relative_url_root
      end
  
      url_base.join + file.local_url + query_params
    end
  
    # Path that is persisted in the tracking Upload model. Used to fetch the
    # upload from the model.
    def upload_paths(identifier)
      file_mounter.blank_uploader.store_dirs.map { |store, path| File.join(path, identifier) }
    end
  
    private
  
    def retrieve_upload_from_batch(identifier)
      BatchLoader.for(identifier: identifier, model: self)
                 .batch(key: self.class) do |upload_params, loader, args|
        model_class = args[:key]
        paths = upload_params.flat_map do |params|
          params[:model].upload_paths(params[:identifier])
        end
  
        Upload.where(uploader: ImageUploader.name, path: paths).find_each do |upload|
          model = model_class.instantiate('id' => upload.model_id)
  
          loader.call({ model: model, identifier: File.basename(upload.path) }, upload)
        end
      end
    end
  
    def add_file_to_batch
      return unless file_mounter
  
      file_mounter.read_identifiers.each { |identifier| retrieve_upload_from_batch(identifier) }
    end
  
    def file_mounter
      strong_memoize(:file_mounter) { _mounter(:file) }
    end
  
    # def clear_file_caches
    #   return unless respond_to?(:verified_emails) && verified_emails.any? && file_changed?
  
    #   Gitlab::FileCache.delete_by_email(*verified_emails)
    # end
  end
  