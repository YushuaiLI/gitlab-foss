# frozen_string_literal: true

class PostTagging < ApplicationRecord
  belongs_to :post_tag
  belongs_to :post
end

PostTagging.prepend_mod_with('PostTagging')