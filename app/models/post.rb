# frozen_string_literal: true

# 用于文件上传
require 'carrierwave/orm/activerecord'

class Post < ApplicationRecord
  extend Gitlab::ConfigHelper

  include Gitlab::ConfigHelper
  include PostCoverPicturable
  include Gitlab::SQL::Pattern

  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_many :post_taggings, dependent: :destroy
  has_many :post_tags, through: :post_taggings
  # 与其他表单的关联  表示  多对1的关系
  belongs_to :user

  # 展示顺序  降序
  # default_scope  lambda { order(created_at: :DESC)}

  # 数据验证
  validates :title,  presence: true, length: { maximum: 140}
  validates :content,  presence: true
  validates :user_id,  presence: true
  validates :lead,  presence: true, length: { maximum: 200}

  extend FriendlyId
  friendly_id :title

  # 标签列表
  def tag_list
    self.post_tags.collect do |tag|
      tag.name
    end.join(",")
  end


  # 从前端创建需要  字段假设  声明传入的字符串
  def tag_list=(post_tags_string)
    # 替换中文标点 小写 去除空格 唯一性
    tag_names = post_tags_string.gsub("，", ",").split(",").collect{|s| s.strip.downcase}.uniq
    new_or_found_tags = tag_names.collect { |tag_name| PostTag.find_or_create_by(name: tag_name) }
    self.post_tags = new_or_found_tags  
  end

  # search  class method
  #  全局搜索
  def Post.search(query)
    fuzzy_search(query, [:title, :lead])
  end
  def Post.search_by_title(query)
    fuzzy_search(query, [:title])
  end

end
Post.prepend_mod_with('Post')