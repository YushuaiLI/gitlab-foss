require 'rails_helper'

RSpec.describe Post, type: :model do

  describe "create valid post" do
    it "should create post successfully" do
      user = create(:user)
      post = user.posts.build(title:"test", content:"this is a test")

      expect(post.save).to be_truthy
      post.reload
      expect(post.title).to eq("test")
    end
  end

  describe "check posts display order" do
    before do
      user = create(:user)
      10.downto 1 do |num|
         Post.create(title: "test#{num}", content: "#{num}条内容", user_id: user.id)       
      end

    end
    it "should be most recent first" do
      expect(Post.first.title).to eq("test1")
    end
  end

  describe "create invalid post" do
    let(:user) { create(:user) }

    it "lacks of title" do
      post = Post.new(title:"", content:"this is a test", user_id: user.id)

      expect(post.save).to be_falsey
    end

    it "lacks of content" do
      post = Post.new(title:"test", content:"", user_id: user.id)

      expect(post.save).to be_falsey
    end

    it "lacks of user_id" do
      post = Post.new(title:"test", content:"this is a test", user_id: nil)

      expect(post.save).to be_falsey
    end

    it "'s title's is too long" do
      invalid_title = "a"*37
      post = Post.new(title:invalid_title, content:"this is a test", user_id: user.id)

      expect(post.save).to be_falsey
    end
  end
end
