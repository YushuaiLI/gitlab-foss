require 'rails_helper'

RSpec.describe StaticPagesController do
    describe "GET home" do
        context "when user is not logged in" do
            it "shows the home page" do
                get :home

                expect(response).to have_http_status(:success)
            end
        end
    end
end