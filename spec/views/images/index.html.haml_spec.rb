require 'rails_helper'

RSpec.describe "images/index", type: :view do
  before(:each) do
    assign(:images, [
      Image.create!(
        file: "File"
      ),
      Image.create!(
        file: "File"
      )
    ])
  end

  it "renders a list of images" do
    render
    assert_select "tr>td", text: "File".to_s, count: 2
  end
end
