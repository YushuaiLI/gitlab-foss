require 'rails_helper'

RSpec.describe "static_pages/home.html.haml", type: :view do
  let(:user) { create(:user) }
  subject { render(template: 'static_pages/home', layout: 'layouts/home_layout') }

  before do
    # mock current_user  current_user_mode
    allow(view).to receive(:current_user).and_return(user)
    allow(view).to receive(:current_user_mode).and_return(Gitlab::Auth::CurrentUserMode.new(user))
  end

  describe "when user is not signed in" do
    let(:user) { nil }
    it 'shows "登录/注册" link' do
      subject
      expect(rendered).to have_link '登录/注册', href: new_user_session_path
    end
  end

  describe "when user is signed in" do
    it 'shows link to user profile' do
      subject
      expect(rendered).to have_link(user.to_reference)
    end 
  end
end
