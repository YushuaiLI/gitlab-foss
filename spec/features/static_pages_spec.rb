require 'rails_helper'

RSpec.describe "StaticPages", type: :feature do
    before do
        visit home_path
    end
    
    describe "click logo to enter home page" do
        it "should return home page whether signed in or not" do
            expect(page).to have_css('.navbar-brand')
            page.find('.navbar-brand').click

            expect(current_path).to eq(home_path)
        end 
    end

    describe "when user is not signed in" do
        it 'click "登录/注册" to enter login page' do
            expect(page).to have_link '登录/注册' # 成功返回了 home 页面

            click_link "登录/注册" 
            expect(current_path).to eq(new_user_session_path)
        end
        
        xit 'click "发布V-blog" to enter new post page' do
            expect(page).to have_link '发布V-blog'

            click_link "发布V-blog"
            expect(current_path).to eq(new_user_session_path)
        end
    end

    describe "when user is signed in" do
        let(:user) { create(:user) }
        before do
            sign_in user
            visit home_path
        end
        it 'click  user reference to enter overview page' do
            expect(page).to have_css(".js-nav-user-dropdown")

            click_link user.to_reference
            expect(current_path).to eq(user_path(user))
        end
        
        xit 'click "发布V-blog" to enter new post page' do
            expect(page).to have_link '发布V-blog'

            click_link "发布V-blog"
            expect(current_path).to eq(new_post_path)
        end
    end
end