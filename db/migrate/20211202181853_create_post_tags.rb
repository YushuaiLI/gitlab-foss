class CreatePostTags < ActiveRecord::Migration[6.1]
  def change
    create_table :post_tags do |t|
      t.string :name

      t.timestamps
    end
    add_index :post_tags, :name
  end
end
