class CreatePostTaggings < ActiveRecord::Migration[6.1]
  def change
    create_table :post_taggings do |t|
      t.references :post_tag, index:true, foreign_key: true
      t.references :post, index:true, foreign_key: true

      t.timestamps
    end
  end
end
